require "active_record"

class Price < ActiveRecord::Base

	belongs_to :event

	self.primary_key = "price_id"

end