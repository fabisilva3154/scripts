#encoding: utf-8
require "nokogiri"
require "net/http"
load    "database_connection.rb"
load    "event.rb"

def get_principal_page(page)
    puts 'http://www.ticketek.com.ar/musica?page='+page
    uri = URI('http://www.ticketek.com.ar/musica?page='+page)
    Net::HTTP.get(uri)
end

def get_event_page(link)
	puts link
    uri = URI(link)
    # Net::HTTP.get(uri)
    r = Net::HTTP.get_response(uri)
    if r.code == "301"
      r = Net::HTTP.get_response(URI.parse(r.header['location']))
    end
    r.body
end




#For each page
(0..19).each do |page_number|
    
    #Get the HTML from the principal page
    principal_page_html = get_principal_page(page_number.to_s)
    
    #Parse HTML
    principal_document = Nokogiri::HTML(principal_page_html)

    #Search the events
    events = principal_document.css('#tktcms-layouts-home-row-bottom-column-left-1-column-bottom .artists-list-item')

    #For each events
    events.each do |event_div|

        #Get title
        title = event_div.css('.artists-list-item-title').text

        #Get url
        url = 'http://ticketek.com.ar'+event_div.css('a').first.attr('href')

        #Get thumbnail
        thumbnail = event_div.css('img').first ? event_div.css('img').attr('src').text : nil

        #Create hash event
        event_hash = { :title => title, :url => url, :site => 'Ticketek', :thumbnail => thumbnail }

        #Get the HTML from the event page
        event_page_html = get_event_page(url)

        #Parse HTML
        event_document = Nokogiri::HTML(event_page_html)

        #Get remaining data
        event_hash[:price] = event_document.css('.price strong').first ? event_document.css('.price strong').first.text : nil
        next_date_text = event_document.css('.next-date') ? event_document.css('.next-date').text : nil
        event_hash[:next_date] = next_date_text ? next_date_text.gsub(/Próxima fecha/,' ').gsub(/Ver todas las fechas y precios/,' ').strip : nil
        event_hash[:place] = event_document.css('.pane-show-venue-data h2').first ? event_document.css('.pane-show-venue-data h2').text : nil


        #Get the prices
        prices = event_document.css('#tickets-sectors .item-list ul li')

        #Create the event object
        event = Event.new(event_hash)


        last_price = ''
        last_place = ''
        
        #For each price
        prices.each do |price|

            price_array = price.text.split(' - ')

            place = price_array.at(0)
            price = price_array.last

            if ( !(( (place.downcase.include?('mesa') || place.downcase.include?('living')) && last_price == price) || (last_place == place && last_price == price)) )

                last_price = price
                last_place = place

                price_hash = { 
                    :place => place, 
                    :price => price
                }

                event.prices.push(Price.new(price_hash))

                puts price_hash
            end

        end

    puts event_hash

    event.save

end
end
