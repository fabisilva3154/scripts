require "nokogiri"
require "net/http"
load    "database_connection.rb"
load    "event.rb"

def get_principal_page
    uri = URI('http://www.lunapark.com.ar/')
    Net::HTTP.get(uri)
end

def get_event_page(link)
	puts link
    uri = URI(link)
    Net::HTTP.get(uri)
end

site = 'lunapark'

#Get the HTML from the principal page
principal_page_html = get_principal_page()

#Parse HTML
principal_document = Nokogiri::HTML(principal_page_html)

#Search the events
events = principal_document.css('.evento-pastilla')

#For each events
events.each do |event_div|

    #Get title
    title = event_div.css('.artista').text

    #Get next date
    next_date = event_div.css('.fechas').text.strip

    #Get url
    url = event_div.css('a').first.attr('href')

    #Create hash event
    event_hash = { :title => title, :next_date => next_date, :url => url, :place => 'Luna Park' }

    #Get the HTML from the event page
    event_page_html = get_event_page(url)

    #Parse HTML
    event_document = Nokogiri::HTML(event_page_html)

    #Get thumbnail
    
    thumbnail = event_document.css('.detalleImagen').first ? event_document.css('.detalleImagen').attr('src').text : nil

    thumbnail = thumbnail ? thumbnail.gsub("..",'http://www.lunapark.com.ar') : nil

    event_hash[:thumbnail] = thumbnail

    #Get location image

    location_image = event_document.css('#recitalx9').first ? event_document.css('#recitalx9').attr('src').text : nil

    event_hash[:location_image] = location_image ? location_image.gsub('..','http://www.lunapark.com.ar') : nil

    #Set site

    event_hash[:site] = site

    #Get the prices
    prices = event_document.css('.breves tr')

    #Create the event object
    event = Event.new(event_hash)

    #For each price
    prices.each do |price|

        # If the row corresponds to a price
        if price.css('td').length != 0

            price_hash = { :place => price.css('th').first.text.strip.gsub(/ +/,' ').gsub(/\n/,''), :price => price.css('td').at(1).text.strip.gsub(/ +/,' ').gsub(/\n/,'').gsub(/.-/,'')}

            event.prices.push(Price.new(price_hash))

            puts price_hash

        end 

    end

    puts event_hash

    event.save

end
