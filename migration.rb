require "active_record"
load    "database_connection.rb"
load    "event.rb"
load    "price.rb"

class CreateEventsTable < ActiveRecord::Migration

  def up
    create_table :events do |t|
      t.string  :title
      t.string  :place
      t.string  :next_date
      t.string  :thumbnail
      t.string  :location_image
      t.string  :price
      t.string  :url
      t.string  :site
    end
    puts 'ran up method'
  end 
  def down
    drop_table :events
  end
end

class CreatePricesTable < ActiveRecord::Migration

  def up
    create_table :prices do |t|
      t.string  :place
      t.string  :price
      t.belongs_to :event
    end
    puts 'ran up method'
  end 
  def down
    drop_table :prices
  end
end

