require "nokogiri"
require "net/http"

def get_principal_page
    uri = URI('http://wtfismyip.com/text')
    Net::HTTP.get(uri)
end

def send_ip(ip)
    uri = URI('http://www.pruebas.com.ar/ip/create_file.php?ip='+ip)
    Net::HTTP.get(uri) 
end

#Get the HTML from the principal page
principal_page_html = get_principal_page()

#Parse HTML
principal_document = Nokogiri::HTML(principal_page_html)

#Get ip
ip = principal_document.text

puts ip

puts 'Sending ip...'

send_ip(ip)

puts 'OK'
