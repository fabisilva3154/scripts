require "active_record"
load    "database_connection.rb"
load    "event.rb"
load    "price.rb"
load    "migration.rb"

CreatePricesTable.migrate(:up)
CreateEventsTable.migrate(:up)
