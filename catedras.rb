require "net/http"
require "nokogiri"

uri = URI.parse("https://catedras.info.unlp.edu.ar/login/index.php")

http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE
http.start
puts 'Entrando a catedras...'
resp, data = http.get('https://catedras.info.unlp.edu.ar/login/index.php')
cookie = resp.response['set-cookie'].split('; ')[0]

request = Net::HTTP::Post.new(uri.request_uri)
request['Cookie'] = cookie
puts 'Obteniendo cookie'
request.set_form_data({"username" => "36069747", "password" => "Queeeeee1", 'rememberusername' => 1})
response = http.request(request)

cookie = response.response['set-cookie'].split('; ')[0]

puts 'Entrando al curso...'
uri = URI.parse("https://catedras.info.unlp.edu.ar/course/view.php?id=460")
request = Net::HTTP::Get.new(uri.request_uri, { 'Cookie' => cookie })

response = http.request(request)

curso_document = Nokogiri::HTML(response.body)
curso_document.css('.resource a').each do |link|
    # puts link.text
    # puts link.attr('href')

    uri = URI.parse(link.attr('href'))
    request = Net::HTTP::Get.new(uri.request_uri)
    request['Cookie'] = cookie
    puts  ''
    print 'Desea bajar "'+link.text+'"? (s/n): '
    bajar = gets.chomp
    if bajar.to_s == 's'
        http.request(request) do |resp|
            link_pdf = Nokogiri::HTML(resp.body)

            puts link_pdf.css('a').at(0).attr('href')

            uri_pdf = URI.parse(link_pdf.css('a').at(0).attr('href'))
            request_pdf = Net::HTTP::Get.new(uri_pdf.request_uri)
            request_pdf['Cookie'] = cookie
            http.request(request_pdf) do |resp_link|
                f = File.open('catedras/'+link.text+'.pdf','w')
                f.write(resp_link.body)
                f.close()
            end
        end
    end
end