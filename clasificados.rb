require "nokogiri"
require "net/http"

def get_principal_page
    uri = URI('http://www.eldia.com.ar/clasificados.aspx?nr=2&ns=4')
    Net::HTTP.get(uri)
end

#Get the HTML from the principal page
principal_page_html = get_principal_page()

#Parse HTML
principal_document = Nokogiri::HTML(principal_page_html)

#Search the events
publicaciones = principal_document.css('.TextoNoticia div hr')

publicaciones.each do |publicacion|
    puts publicacion.previous.text.scan(/\$\ *\d+\.*\d+/)
    puts publicacion.previous.text
    puts '----------'
end

