require "active_record"
load    "price.rb"


class Event < ActiveRecord::Base
	
	has_many :prices

	self.primary_key = "event_id"

end